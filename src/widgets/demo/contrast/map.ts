import * as mars3d from "mars3d"

let map: mars3d.Map // 地图对象
let mapEx: mars3d.Map
let graphicLayer: mars3d.GraphicLayer
let tileLayer: mars3d.TilesetLayer
let Cesium: any

// 初始化当前业务
export function onMounted(mapInstance: mars3d.Map): void {
  map = mapInstance // 记录map

  const centerDivEx = mars3d.DomUtil.create("div", "", document.body)
  centerDivEx.setAttribute("id", "centerDivEx")
  const sourceContainer = mars3d.DomUtil.create("div", "mars3d-container", centerDivEx)
  sourceContainer.setAttribute("id", "mars3dContainerEx")

  // 获取原来地图的参数
  const mapOptions2 = map.getOptions() // map.getCurrentOptions()
  mapOptions2.control.baseLayerPicker = true // basemaps底图切换按钮
  mapOptions2.control.sceneModePicker = false

  // 用于双屏同图层，不同配置展示
  for (let i = 0, len = mapOptions2.layers.length; i < len; i++) {
    const item = mapOptions2.layers[i]
    if (item.compare) {
      for (const key in item.compare) {
        item[key] = item.compare[key] // 存在compare属性时
      }
    }
  }
  console.log("分屏地图配置", mapOptions2)

  mapEx = new mars3d.Map(sourceContainer, mapOptions2)
  // mapEx.basemap = "天地图电子"

  // 场景模式(2D/3D/哥伦布)变换完成
  map.on(mars3d.EventType.morphComplete, function (event) {
    if (map.scene.mode === Cesium.SceneMode.SCENE2D) {
      mapEx.scene.screenSpaceCameraController.enableTilt = false
    } else {
      mapEx.scene.screenSpaceCameraController.enableTilt = true
    }
  })

  map.on(mars3d.EventType.cameraChanged, _map_extentChangeHandler)
  map.camera.percentageChanged = 0.01

  mapEx.on(mars3d.EventType.cameraChanged, _mapEx_extentChangeHandler)
  mapEx.camera.percentageChanged = 0.01

  _map_extentChangeHandler()

  // _loadTileModel("https://wckj2020.obs.myhuaweicloud.com:443/wckj/contrast/2019/24-2/tileset.json", 4, 0, map)
  // _loadTileModel("http://192.168.1.47:8098/2019/3dtiles/tileset.json", 1, 0, map)
  // _loadTileModel("https://wckj2020.obs.myhuaweicloud.com:443/wckj/contrast/2021/Scene/24m2m3dtiles.json", 1.5, -32, mapEx)
  // _loadTileModel("http://192.168.1.47:8098/2021/24-2-3dtiles/Scene/24m2m3dtiles.json", 2, -32, mapEx)

  _loadBuildingShp(map)
  _loadBuildingShp(mapEx)
}

// 释放当前业务
export function onUnmounted(): void {
  map.graphicLayer.clear()
  map = null
}

function _map_extentChangeHandler() {
  mapEx.off(mars3d.EventType.cameraChanged, _mapEx_extentChangeHandler)

  updateView(map, mapEx)
  mapEx.on(mars3d.EventType.cameraChanged, _mapEx_extentChangeHandler)
}

function _mapEx_extentChangeHandler() {
  map.off(mars3d.EventType.cameraChanged, _map_extentChangeHandler)

  updateView(mapEx, map)
  map.on(mars3d.EventType.cameraChanged, _map_extentChangeHandler)
}

// “变化屏”mapChange变化，将“被更新屏”mapUpdate同步更新
function updateView(mapChange, mapUpdate) {
  const view = mapChange.getCameraView()
  mapUpdate.setCameraView(view, { duration: 0 })
}

// 变化图斑加载
function _loadBuildingShp(_target) {
  graphicLayer = new mars3d.layer.GeoJsonLayer({
    name: "建筑变化图斑",
    url: "http://120.79.221.215:8080/geoserver/dg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dg%3Atx&maxFeatures=200&outputFormat=application%2Fjson",
    // url: "http://120.79.221.215:8080/geoserver/dg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dg%3Asp&maxFeatures=150&outputFormat=application%2Fjson",
    symbol: {
      styleOptions: {
        color: "#fff",
        outlineColor: "#fff",
        opacity: 0.4,
        clampToGround: true,
        // 高亮时的样式
        highlight: {
          opacity: 0.5
        },
        label: {
          // 面中心点，显示文字的配置
          text: "{tbbh}", // 对应的属性名称
          opacity: 1,
          font_size: 20,
          color: "#ffffff",

          font_family: "楷体",
          outline: true,
          outlineColor: "#000000",
          outlineWidth: 3,

          background: true,
          backgroundColor: "#000000",
          backgroundOpacity: 0.3,

          font_weight: "normal",
          font_style: "normal",

          scaleByDistance: true,
          scaleByDistance_far: 20000000,
          scaleByDistance_farValue: 0.1,
          scaleByDistance_near: 1000,
          scaleByDistance_nearValue: 1,

          distanceDisplayCondition: false,
          distanceDisplayCondition_far: 10000,
          distanceDisplayCondition_near: 0,
          visibleDepth: false
        }
      },
      callback: function (attr, styleOpt) {
        switch (attr.JZBHLX) {
          case "拆除未建":
            styleOpt.color = "#fff"
            break
          case "新增":
            styleOpt.color = "#ff0000"
            break
          case "加建":
            styleOpt.color = "#ffff00"
            break
          case "拆除重建":
            styleOpt.color = "#0000ff"
            break
        }
        // const diffHeight = Number(attr.floors || 1) * Number(attr.flo_height)
        // return { height: 0, diffHeight: diffHeight }
      }
    },
    center: { lat: 22.841682, lng: 113.971412, alt: 77, heading: 271, pitch: -64 },
    popup: [
      { field: "村", name: "所属村" },
      { field: "tbbh", name: "变化图斑编号" },
      { field: "JZBHLX", name: "建筑变化类型" },
      { field: "新增面", name: "改扩建面积(m²)" },
      { field: "zdmj", name: "新建建筑占地面积(m²)" },
      { field: "备注", name: "备注" }
    ],
    flyTo: true
  })

  _target.addLayer(graphicLayer)

  // 绑定事件
  graphicLayer.on(mars3d.EventType.load, function (event) {
    console.log("数据加载完成", event)
  })
}

// 模型加载
function _loadTileModel(_url: string, _accuracy: number, _height: number, _target: any): void {
  tileLayer = new mars3d.layer.TilesetLayer({
    url: _url,
    maximumScreenSpaceError: _accuracy,
    skipLevelOfDetail: true,
    flyTo: true,
    clampToGround: false,
    preloadFlightDestinations: true,
    preferLeaves: false,
    dynamicScreenSpaceError: false,
    foveatedScreenSpaceError: true
    // position: {
    //   alt: _height
    // }
  })
  _target.addLayer(tileLayer)

  tileLayer.on(mars3d.EventType.load, function (event) {
    console.log("倾斜模型加载完成,模型位置:" + event.target.center)
  })
}

// // 绘制矩形（演示map.js与index.vue的交互）
// export function drawExtent(): Promise<any> {
//   return new Promise((resolve) => {
//     map.graphicLayer.clear()
//     // 绘制矩形
//     map.graphicLayer.startDraw({
//       type: "rectangle",
//       style: {
//         fill: true,
//         color: "rgba(255,255,0,0.2)",
//         outline: true,
//         outlineWidth: 2,
//         outlineColor: "rgba(255,255,0,1)"
//       },
//       success: function (graphic: any) {
//         const rectangle = mars3d.PolyUtil.formatRectangle(graphic._rectangle_draw)
//         resolve({ extent: JSON.stringify(rectangle) })
//       }
//     })
//   })
// }
