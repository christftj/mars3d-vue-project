import { defineAsyncComponent, markRaw } from "vue"
import { WidgetState } from "@mars/common/store/widget"
import { StoreOptions } from "vuex"

const store: StoreOptions<WidgetState> = {
  state: {
    widgets: [
      {
        component: markRaw(defineAsyncComponent(() => import("@mars/widgets/demo/menu/index.vue"))),
        name: "menu",
        autoDisable: false,
        disableOther: false
      },
      {
        component: markRaw(defineAsyncComponent(() => import("@mars/widgets/demo/sample-pannel/index.vue"))),
        name: "sample-pannel"
      },
      {
        component: markRaw(defineAsyncComponent(() => import("@mars/widgets/demo/sample-dialog/index.vue"))),
        name: "sample-dialog"
      },
      {
        component: markRaw(defineAsyncComponent(() => import("@mars/widgets/demo/ui/index.vue"))),
        name: "ui"
      },
      {
        name: "my-widget",
        component: markRaw(defineAsyncComponent(() => import(/* webpackChunkName: "my-widget" */ "@mars/widgets/demo/my-dialog/index.vue")))
      },
      {
        name: "contrast-widget",
        component: markRaw(defineAsyncComponent(() => import(/* webpackChunkName: "contrast-widget" */ "@mars/widgets/demo/contrast/index.vue")))
      },
      {
        name: "contrast-title",
        component: markRaw(defineAsyncComponent(() => import(/* webpackChunkName: "contrast-widget" */ "@mars/widgets/demo/contrast/pageTitle.vue")))
      },
      {
        name: "contrast-layer",
        component: markRaw(
          defineAsyncComponent(() => import(/* webpackChunkName: "contrast-widget" */ "@mars/widgets/demo/contrast/switchLayer.vue"))
        )
      }
    ],
    openAtStart: ["contrast-title", "contrast-widget", "contrast-layer"]
  }
}

export default store
